import ballerinax/kafka;
import ballerina/log;
import ballerina/io;
import ballerina/kubernetes;

@kubernetes:Ingress {
    hostname: "localhost"
}
@kubernetes:Service {
    name:"courseOutlineHod"
}

@kubernetes:Deployment {
    livenessProbe: true,
    dockerHost: "tcp://192.168.99.100:2376",
    dockerCertPath: "/home/Saimah/.minikube/certs"
}
@http:ServiceConfig {
    basePath: "/lecturer"
}

string topicCourseOutlineHod = "course-outline-hod";
string topicCourseOutlineLecturer = "course-outline-lecturer";

//defining kafka consumer configurations
kafka:ConsumerConfiguration consumerConfigs = {
    groupId: "course-outline-consumer",
    topics: [topicCourseOutlineHod]
};

//difining kafka consumer listener
listener kafka:Listener lecturerConsumer = new (kafka:DEFAULT_URL, consumerConfigs);

//defining kafka producer configurations
kafka:ProducerConfiguration producerConfigs = {
    clientId: "course-lecture-consumer"
};

kafka:Producer lecturerProducer = check new (kafka:DEFAULT_URL, producerConfigs);

//course outline record type
type CourseInfo record {
    string facultyName;
    string department;
    string courseName;
    string courseCode;
    string lecturer;
};

//lecturer actions
type Actions record {
    boolean isViewed?;
    boolean isGenerated?;
    CourseInfo informationInput?;
};

# Description  
service kafka:Service on lecturerConsumer {
    remote function onConsumerRecord(kafka:Caller caller, kafka:ConsumerRecord[] records)returns error? {
        //reading data from a kafka topic
        foreach var entry in records {
            byte[] serializeMsg = entry.value;
            string msg = serializeMsg.toString();

            check processKafkaRecord(entry);

            io:StringReader recievedString = new (msg);
            var recievedCourseOutlineJson = recievedString.readJson();

            if (recievedCourseOutlineJson is error) {
                io:print("Error parsing json");
            } else {
                string topicToPublish = "";
                var recievedCourseOutline = Actions;
            }
        }
    }
}


function  processKafkaRecord(kafka:ConsumerRecord kafkaRecord) returns  error?{
    byte[] value = kafkaRecord.value;

    string messageContent = check string:fromBytes(value);
    log:printInfo("Recieved Message: "+messageContent);
}