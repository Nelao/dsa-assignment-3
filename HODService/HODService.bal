import ballerina/http;
import ballerina/io;
import ballerinax/kafka;
import ballerina/kubernetes;

@kubernetes:Ingress {
    hostname: "localhost"
}
@kubernetes:Service {
    name:"courseOutlineHod"
}

@kubernetes:Deployment {
    livenessProbe: true
}
@http:ServiceConfig {
    basePath: "/hod"
}

//creating a kafka server
kafka:Producer hodProducer = check new (kafka:DEFAULT_URL);

//course outline record type
type CourseInfo record {
    string facultyName;
    string department;
    string courseName;
    string courseCode;
    string lecturer;
};

//hod actions
type Actions record {
    boolean isReviewed?;
    boolean isAproved?;
    CourseInfo informationInput?;
};

//listening on port 8080
listener http:Listener hodLIstener = new (8080);

# Description  
@http:ServiceConfig {basePath: "/hod"}
service hod on hodLIstener {
    @http:ResourceConfig {
        methods: ["PUT"],
        path: "/review"
    }
    //course outline review resource function
    resource function get courseOutlineReview(http:Caller caller, http:Request request) {
        string topic = "course-outline-hod";
        json message;

        //dclaring error and success message
        json reviewSuccessResponse = {success: "true", message: "successfully reviewed!"};
        json reviewFailureResponse = {success: "false", message: "review failed"};

        //creating an http response
        http:Response resp = new;

        //creating a request payload
        var requestPayload = request.getJsonPayload();

        if (requestPayload is error) {
            resp.setJsonPayload(reviewFailureResponse);

            //sending the failure response
            _ = caller->respond(resp);
        } else {
            message = requestPayload;

            any answer = io:readln("is reviewed?(yes/no): ");

            if (answer == "yes") {
                string header = message.toString();
                resp.setJsonPayload(reviewSuccessResponse);

                //sending the success response
                _ = caller->respond(resp);
            }

            //converting the user input to byte
            byte[] messageToPublish = answer.toString().toBytes();

            //publishing message to kafka
            var result = hodProducer->send({topic: topic, value: messageToPublish});
            if (result is error) {
                io:print("Sending review failed");
            } else {
                io:print("Review recieved");
            }
        }
    }

    @http:ResourceConfig {
        methods: ["PUT"],
        path: "/approve"
    }
    //course outline review resource function
    resource function get courseOutlineApproval(http:Caller caller, http:Request request) {
        string topic = "course-outline-hod";
        json message;

        //dclaring error and success message
        json approveSuccessResponse = {success: "true", message: "successfully reviewed!"};
        json approveFailureResponse = {success: "false", message: "review failed"};

        //creating an http response
        http:Response resp = new;

        //creating a request payload
        var requestPayload = request.getJsonPayload();

        if (requestPayload is error) {
            resp.setJsonPayload(approveFailureResponse);

            //sending the failure response
            _ = caller->respond(resp);
        } else {
            message = requestPayload;

            any answer = io:readln("is approved?(yes/no): ");

            if (answer == "yes") {
                string header = message.toString();
                resp.setJsonPayload(approveSuccessResponse);

                //sending the success response
                _ = caller->respond(resp);
            }

            //converting the user input to byte
            byte[] messageToPublish = answer.toString().toBytes();

            //publishing message to kafka
            var result = hodProducer->send({topic: topic, value: messageToPublish});
            if (result is error) {
                io:print("Sending approval failed");
            } else {
                io:print("Approval recieved");
            }
        }
    }

    @http:ResourceConfig {
        methods: ["POST"],
        path: "/infoinput"
    }
    //course outline review resource function
    resource function post courseOutlineInfoInput(http:Caller caller, http:Request request) {
        string topic = "course-outline-hod";
        json message;

        //dclaring error and success message
        json inputSuccessResponse = {success: "true", message: "successfully reviewed!"};
        json inputFailureResponse = {success: "false", message: "review failed"};

        //creating an http response
        http:Response resp = new;

        //creating a request payload
        var requestPayload = request.getJsonPayload();

        if (requestPayload is error) {
            resp.setJsonPayload(inputFailureResponse);

            //sending the failure response
            _ = caller->respond(resp);
        } else {
            message = requestPayload;

            string header = message.toString();
            resp.setJsonPayload(inputSuccessResponse);
            _ = caller->respond(resp);

            if (header == "infoinput") {
                var infoinput = CourseInfo;

                if (infoinput is CourseInfo) {
                    Actions actions = {};
                    actions.informationInput = infoinput;

                    var msg = actions.toJson();
                    if (msg is error) {
                        io:print("Sending approval failed");
                    } else {
                        byte[] messageToPublish = msg.toString().toBytes();
                        var result = hodProducer->send({
                            topic: topic,
                            value: messageToPublish
                        });

                        if (result is error) {
                            io:print("Sending course input failed");
                        } else {
                            io:print("Course input recieced!");
                        }
                    }
                }
            }
        }
    }
}
